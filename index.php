<?php
$token = 'ВАШ_ТОКЕН';
//Отримати токен можна тут https://lntu.pp.ua/accounts
$json = file_get_contents("http://lntu.pp.ua/api/user?token=".$token);
$data = json_decode($json, true);

$random = mt_rand();

$name = $data['data']['login'];
$email = $data['data']['email'];
$avatar = $data['data']['picture'];
$money = $data['data']['money'];
$point = $data['data']['point'];
$rating = $data['data']['rating'];

?>

<html>
    <head>
        <title>LNTU API</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="lntu.css?lntu=<?php echo $random;?>">
    </head>
    <body>
        <div class="head"><div class="name">LNTU API</div></div> 
        <div style="padding: 30px;"></div>
        <div class="vidstup">
        <div class="content">
            
            <div class="block1"><img class="avatar" src="<?php echo $avatar;?>"></div>
            <div class="block2"><center><h1><?php echo $name;?></h1><br>
            Монети: <?php echo $money;?><br>
            Бали: <?php echo $point;?><br>
            Реййтинг: <?php echo $rating;?><br>
            </center></b></div>
            
        </div>
        </div>
    </body> 
</html>
